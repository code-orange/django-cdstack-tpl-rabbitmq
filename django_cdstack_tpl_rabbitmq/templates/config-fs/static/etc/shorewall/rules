#
# Shorewall -- /etc/shorewall/rules
#
# For information on the settings in this file, type "man shorewall-rules"
#
# The manpage is also online at
# http://www.shorewall.net/manpages/shorewall-rules.html
#
##############################################################################################################################################################
#ACTION		SOURCE		DEST		PROTO	DPORT	SPORT	ORIGDEST	RATE	USER	MARK	CONNLIMIT	TIME	HEADERS	SWITCH	HELPER

?SECTION ALL
?SECTION ESTABLISHED
?SECTION RELATED
?SECTION INVALID
?SECTION UNTRACKED
?SECTION NEW
# Allow ping to the host
Ping(ACCEPT) all		fw

# Drop invalid tcp packets
Invalid(DROP)   net     all         tcp

# Block SMB to mitigate NTLM attacks
REJECT          all     net         tcp     137,139,445
REJECT          all     net         udp     137,138

# Allow SSH to the host
ACCEPT		all			fw			tcp		{{ ssh_port }}

# Allow HTTP access
ACCEPT		net			fw			tcp		80
ACCEPT		net			fw			tcp		443

{% if rabbitmq_amqp_enable == "true" %}
# Allow AMQP access
#ACCEPT		net			fw			tcp		{% if rabbitmq_listener_tcp_default %}{{ rabbitmq_listener_tcp_default }}{% else %}5672{% endif %}
ACCEPT		net			fw			tcp		{% if rabbitmq_listener_ssl_default %}{{ rabbitmq_listener_ssl_default }}{% else %}5671{% endif %}
{% endif %}

{% if rabbitmq_mqtt_enable == "true" %}
# Allow MQTT access
#ACCEPT		net			fw			tcp		{% if rabbitmq_mqtt_listener_tcp_default %}{{ rabbitmq_mqtt_listener_tcp_default }}{% else %}1883{% endif %}
ACCEPT		net			fw			tcp		{% if rabbitmq_mqtt_listener_ssl_default %}{{ rabbitmq_mqtt_listener_ssl_default }}{% else %}8883{% endif %}
{% endif %}

# Allow incoming SNMP query from trap server
?if __IPV4
{% for monitoring_snmp_trap_receiver in monitoring_snmp_trap_receivers %}ACCEPT      all:{{ monitoring_snmp_trap_receiver.ip }}       fw          udp     161
{% endfor %}?endif
?if __IPV6
#{% for monitoring_snmp_trap_receiver in monitoring_snmp_trap_receivers %}ACCEPT      all:{{ monitoring_snmp_trap_receiver.ip6 }}       fw          udp     161
{% endfor %}?endif


# IP Whitelist outgoing
?if __IPV4
{% for fw_white_list_net in fw_white_list_ips %}
ACCEPT		all!net		all:{{ fw_white_list_net }}
{% endfor %}
?endif
?if __IPV6
{% for fw_white_list_net in fw_white_list_ip6s %}
ACCEPT		all!net		all:[{{ fw_white_list_net }}]
{% endfor %}
?endif
